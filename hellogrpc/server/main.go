package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "gitlab.com/sanaftg/grpc_demo/hellogrpc/hellogrpc"
	"google.golang.org/grpc"
)

// gRPC server struct
type server struct {
}

// .protoで定義したGreetServerを定義している
func (s *server) GreetServer(ctx context.Context, p *pb.GreetRequest) (*pb.GreetMessage, error) {
	log.Printf("Request from: %s", p.Name)
	return &pb.GreetMessage{Msg: fmt.Sprintf("Hello, %s. ", p.Name)}, nil
}

func main() {
	// gRPC
	port := 10000
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Printf("Run server port: %d", port)
	grpcServer := grpc.NewServer()
	// メソッドを定義
	pb.RegisterHelloGrpcServer(grpcServer, &server{})
	// gRPCサーバを公開
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
