@echo off
set TOOLDIR=%GOPATH%\bin\Grpc.Tools\tools\windows_x64\
set PROJECTNAME=%1
set PROTOPATH=%PROJECTNAME%\%PROJECTNAME%.proto
@if "%1"=="" (
    echo please input projectname
    exit /b 0
)
if not exist %TOOLDIR% (
    echo Not Found %TOOLDIR%
    exit /b 0
)
if not exist %PROTOPATH% (
    echo Not Found %PROTOPATH%
    exit /b 0
)
call %TOOLDIR%protoc.exe -I %PROJECTNAME% --csharp_out %PROJECTNAME% --grpc_out %PROJECTNAME% --plugin=protoc-gen-grpc=%TOOLDIR%grpc_csharp_plugin.exe %PROTOPATH%
if %errorlevel%==0 (
echo Csharp Complete!
) else (
echo Csharp Failure...
)
call %TOOLDIR%protoc.exe -I %PROJECTNAME%/ %PROTOPATH% --go_out=plugins=grpc:%PROJECTNAME%
if %errorlevel%==0 (
echo Go Complete!
) else (
echo Go Failure...
)