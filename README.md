# Grpcを使ったUnityとGoサーバーの通信
Unity用のパッケージが公式からビルドされるようになったので、UnityクライアントとGoサーバーを使った簡単な通信基盤を作りたい。

## Go環境構築
適用にやる。
https://qiita.com/maruyama42/items/7b6cd82eb37e2a524572
パス通すの忘れずに

## Go環境でのGRPCの準備
https://qiita.com/Keiwa/items/6b375c1367fa392c96bc

あんまりはまらなかったので覚えてない。
上のチュートリアルはわかりやすかった。

## Unity用のGRPC環境の準備
https://qiita.com/muroon/items/2115c2c72be8b0c3f5f6

とりあえず最新の突っ込んだ。

## ProtoBufferのC#ソースを自動生成する
滅茶苦茶、陥った


### Grpc.ToolsをNugetからインストール
https://qiita.com/muroon/items/4e12dde47b9e8b1e94d3

```
$ Grpc.Tools.1.18.0\tools\windows_x64\protoc.exe -I {プロジェクト} --csharp_out {C#用ソースを生成するとこ} --grpc_out {C#用ソースを生成するとこ} --plugin=protoc-gen-grpc=Grpc.Tools.1.18.0\tools\windows_x64\grpc_csharp_plugin.exe {.protoファイル}
```