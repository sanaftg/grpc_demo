package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"

	"log"

	"google.golang.org/grpc"

	pb "gitlab.com/sanaftg/grpc_demo/grpcunity/grpcunity"
)

// gRPC struct
type server struct {
	contents []message
}

// チャットメッセージ
type message struct {
	author  string
	content string
}

// Greet
func (s *server) GreetServer(ctx context.Context, p *pb.GreetRequest) (*pb.GreetMessage, error) {
	log.Printf("Request from: %s", p.Name)
	return &pb.GreetMessage{Msg: fmt.Sprintf("Hello, %s. ", p.Name)}, nil
}

// チャットルームへstreamを使いメッセージを送信する
func (s *server) SendMessage(stream pb.GrpcUnity_SendMessageServer) error {
	// 無限ループ
	for {
		// クライアントからメッセージ受信
		m, err := stream.Recv()
		//log.Printf("Receive message>> [%s] %s", m.Name, m.Content)
		// EOF、エラーなら終了
		if err == io.EOF {
			// EOFなら接続終了処理
			return stream.SendAndClose(&pb.SendResult{
				Result: true,
			})
		}
		if err != nil {
			return err
		}
		// 終了コマンド
		if m.Content == "/exit" {
			return stream.SendAndClose(&pb.SendResult{
				Result: true,
			})
		}
		// メッセージの追加
		s.contents = append(
			s.contents,
			message{
				author:  m.Name,
				content: m.Content,
			},
		)
	}
}

// チャットルームの新着メッセージをstreamを使い配信する
func (s *server) GetMessages(p *pb.MessagesRequest, stream pb.GrpcUnity_GetMessagesServer) error {

	// 差を使って新着メッセージを検知する
	previousCount := len(s.contents)
	currentCount := 0
	// 無限ループ
	for {
		currentCount = len(s.contents)
		// 現在のmessageCountが前回より多ければ新着メッセージあり
		if previousCount < currentCount {
			msg, _ := latestMessage(s.contents)
			// クライアントへメッセージ送信
			if err := stream.Send(&pb.Message{Id: "test", Name: msg.author, Content: msg.content}); err != nil {
				return err
			}
		}
		previousCount = currentCount
	}
}

func latestMessage(messages []message) (message, error) {
	length := len(messages)
	if length == 0 {
		return message{}, errors.New("Not found")
	}
	return messages[length-1], nil
}

func main() {
	// gRPC
	port := 10000
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))
	if err != nil {
		log.Fatalf("lfailed to listen: %v", err)
	}
	log.Printf("Run server port: %d", port)
	grpcServer := grpc.NewServer()
	pb.RegisterGrpcUnityServer(grpcServer, &server{contents: []message{}})
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
